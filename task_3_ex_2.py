"""
Write a function converting a Roman numeral from a given string N into an Arabic numeral.
Values may range from 1 to 100 and may contain invalid symbols.
Invalid symbols and numerals out of range should raise ValueError.

Numeral / Value:
I: 1
V: 5
X: 10
L: 50
C: 100

Example:
N = 'I'; result = 1
N = 'XIV'; result = 14
N = 'LXIV'; result = 64

Example of how the task should be called:
python3 task_3_ex_2.py LXIV

Note: use `argparse` module to parse passed CLI arguments
"""
import argparse

# Constanta of the compliance Roman numeral to Arabic.
LITERALS = {
    'I': 1,
    'V': 5,
    'X': 10,
    'L': 50,
    'C': 100
}


def from_roman_numerals(args: str) -> int:
    """Transforming Roman numerals to Arabic.

    :param args: Roman numeral as string.
    :return: Integer number.
    """
    # Check for a blank string
    if not args:
        raise ValueError
    result = 0
    args = args[::-1]   # Reverse the string.

    '''
    Looking through the characters of a string to match Roman numerals.
    Subject to the validity of char and depending on the previous character
    adding or subtracting the corresponding number.'''
    for index, element in enumerate(args):
        if element not in LITERALS:
            raise ValueError
        if not index:
            result += LITERALS.get(element)
        elif LITERALS.get(element) >= LITERALS.get(args[index - 1]):
            result += LITERALS.get(element)
        else:
            result -= LITERALS.get(element)

    return result


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("n", type=str, help='number')
    args = parser.parse_args()
    print(from_roman_numerals(args.n))


if __name__ == "__main__":
    main()
