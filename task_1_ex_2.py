"""01-Task1-Task2
Write a Python-script that performs the standard math functions on the data. The
name of function and data are set on the command line when the script is run.
The script should be launched like this:
$ python my_task.py add 1 2

Notes:
Function names must match the standard mathematical, logical and comparison
functions from the built-in libraries.
The script must raises all happened exceptions.
For non-mathematical function need to raise NotImplementedError.
Use the argparse module to parse command line arguments. Your implementation
shouldn't require entering any parameters (like -f or --function).
"""
import argparse
import math
import operator


def calculate(args: argparse.Namespace):
    """Receive a list with the name of the standard math functions and data for
    it, return the result of math function.

    :param args:
    :return: the result of math function.
    """
    if args.operator in dir(math):
        return getattr(math, args.operator)(*args.arguments)
    elif args.operator in dir(operator):
        return getattr(operator, args.operator)(*args.arguments)
    else:
        raise NotImplementedError


def main():
    parser = argparse.ArgumentParser(description='Simple arithmetic operations')
    parser.add_argument('operator', type=str, help='Operator')
    parser.add_argument('arguments', nargs='+',
                        type=float, help='List of float arguments')
    arguments = parser.parse_args()
    print(calculate(arguments))


if __name__ == '__main__':
    main()
