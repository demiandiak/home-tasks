"""01-Task1-Task1
Write a Python-script that performs simple arithmetic operations:
'+', '-', '*', '/'. The type of operator and data are set on the command line
when the script is run.
The script should be launched like this:
$ python my_task.py 1 * 2

Notes:
For other operations need to raise NotImplementedError.
Do not dynamically execute your code (for example, using exec()).
Use the argparse module to parse command line arguments.
Your implementation shouldn't require entering any parameters
(like -f or --function).
"""
import argparse


def calculate(args: argparse.Namespace) -> float:
    """The function is takes a list of three arguments and try to perform
    simple arithmetic operations: '+', '-', '*', '/'.

    :param args: list of three arguments [First operand, Operator, Second
                operand].
    :return: the result of calculations.
    """
    funcs = {
        '+': lambda x, y: x + y,
        '-': lambda x, y: x - y,
        '*': lambda x, y: x * y,
        '/': lambda x, y: x / y
    }

    if func := funcs.get(args.operator):
        return func(args.first, args.second)
    else:
        raise NotImplementedError


def main():
    parser = argparse.ArgumentParser(description='Simple arithmetic operations')
    parser.add_argument('first', type=float, help='First operand')
    parser.add_argument('operator', type=str, help='Operator')
    parser.add_argument('second', type=float, help='Second operand')
    arguments = parser.parse_args()
    print(calculate(arguments))


if __name__ == '__main__':
    main()
