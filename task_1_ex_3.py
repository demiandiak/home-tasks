""" Write a Python-script that determines whether the input string is the
correct entry for the 'formula' according EBNF syntax (without using regular
expressions).
Formula = Number | (Formula Sign Formula)
Sign = '+' | '-'
Number = '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'
Input: string
Result: (True / False, The result value / None)

Example,
user_input = '1+2+4-2+5-1' result = (True, 9)
user_input = '123' result = (True, 123)
user_input = 'hello+12' result = (False, None)
user_input = '2++12--3' result = (False, None)
user_input = '' result = (False, None)

Example how to call the script from CLI:
python task_1_ex_3.py 1+5-2

Hint: use argparse module for parsing arguments from CLI
"""
import argparse


def check_formula(user_input: str) -> tuple:
    """Receive the string with mathematical expression check it for correctness
    and return the result of expression.

    :param user_input: string with mathematical expression.
    :return: Tuple of (True / False, The result value / None).
    """
    if not user_input or not user_input[0].isdigit():
        return False, None

    def inner_func(string: str):
        if not string:
            return 0
        elif string.isdigit():
            return int(string)
        else:
            index = 1
        while string[index].isdigit() and index < (len(string)-1):
            index += 1
        if index == (len(string)-1):
            return int(string)
        return int(string[:index]) + inner_func(string[index:])

    try:
        return True, inner_func(user_input)
    except ValueError:
        return False, None


def main():
    parser = argparse.ArgumentParser('Enter your formula')
    parser.add_argument('user_input', type=str, help='my expression')
    arguments = parser.parse_args()
    print(check_formula(arguments.user_input))


if __name__ == '__main__':
    main()
