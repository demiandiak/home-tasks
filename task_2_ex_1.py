"""
Task 2_3

You are given n bars of gold with weights: w1, w2, ..., wn and bag with
capacity W. There is only one instance of each bar and for each bar you can
either take it or not (hence you cannot take a fraction of a bar). Write a
function that returns the maximum weight of gold that fits into a knapsack's
capacity.

The first parameter contains 'capacity' - integer describing the capacity of a
knapsack.
The next parameter contains 'weights' - list of weights of each gold
bar.
The last parameter contains 'bars_number' - integer describing the number
of gold bars.
Output : Maximum weight of gold that fits into a knapsack with
capacity of W.

Note:
Use the argparse module to parse command line arguments. You don't need to
enter names of parameters (i. e. -capacity)
Raise ValueError in case of false parameter inputs
Example of how the task should be called:
python3 task3_1.py -W 56 -w 3 4 5 6 -n 4
"""
import argparse


def bounded_knapsack(args: argparse.Namespace) -> int:
    """This function finds the sum of largest set of the given, but not more
    than the given limit.

    :param args: list of argparse parameters
    :return: largest weight
    """
    knapsack_free_weights = args.W
    bars = args.w
    number_of_bars = args.n
    bars.sort()

    for el in bars + [number_of_bars, knapsack_free_weights]:
        if el <= 0:
            raise ValueError

    possible_knapsack = [1] + [0] * knapsack_free_weights

    for j in range(number_of_bars):
        for i in range(knapsack_free_weights, bars[j] - 1, -1):
            if possible_knapsack[i - bars[j]] == 1:
                possible_knapsack[i] = 1

    max_weights = knapsack_free_weights
    while possible_knapsack[max_weights] == 0:
        max_weights -= 1

    return max_weights


def main():
    parser = argparse.ArgumentParser('Enter your formula')
    parser.add_argument('-W', type=int, help='The capacity of a knapsack.')
    parser.add_argument('-w', type=int, nargs='+',
                        help='List of weights of each gold bar.')
    parser.add_argument('-n', type=int, help='The number of gold bars.')
    arguments = parser.parse_args()
    print(bounded_knapsack(arguments))


if __name__ == '__main__':
    main()
